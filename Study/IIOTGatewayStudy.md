## IIOT Gateway

- IIoT stands for the Industrial Internet of Things or Industrial IoT that initially mainly referred to an industrial framework whereby a large number of devices or machines are connected and synchronized through the use of software tools and third platform technologies in a machine-to-machine and Internet of Things context, later an Industry 4.0 or Industrial Internet context.

And also IIoT is defined as “machines, computers and people enabling intelligent industrial operations using advanced data analytics for transformational business outcomes”.

- It refers to interconnected sensors, instruments, and other devices networked together with computers' industrial applications, including manufacturing and energy management.

![](https://www.i-scoop.eu/wp-content/uploads/2017/01/Industrial-Internet-of-Things-2.jpg.webp)

# IIoT applications and examples:

- In a real-world IIoT deployment of smart robotics, ABB, a power and robotics firm, is using connected sensors to monitor the maintenance needs of its robots to prompt repairs before parts break.

![](https://cdn.ttgtmedia.com/rms/onlineimages/iota-industry_iot_apps_desktop.png)

# Architecture:

- IIoT systems are usually conceived as a layered modular architecture of digital technology.

- The device layer refers to the physical components: CPS, sensors or machines. 

- In this IIOT,these having 4 layers in architecture.

Content layer : User interface devices (e.g. screens, tablets, smart glasses)
Service layer :	Applications, software to analyze data and transform it into information
Network layer :	Communications protocols, wifi, cloud computing
Device layer :	Hardware: CPS, machines, sensors

![](https://cdn.ttgtmedia.com/rms/onlineimages/iota-industry_iot_apps_desktop.png)


# communication protocols

- A communication protocol is a system of rules that allow two or more entities of a communications system to transmit information via any kind of variation of a physical quantity. The protocol defines the rules, syntax, semantics and synchronization of communication and possible error recovery methods.
- **MODBUS AND MQTT** are the communication protocols of IIOT.
- There are 4 most communication protocols in Industrial IOT. They are given below:
1.WiFi
2.SATELLITE COMMUNICATION
3.NFC AND RFID
4.Bluetooth

# cloud computing
- Cloud systems have three types of flows: between devices and applications deployed at the "edge"; between the edge and cloud (traditional M2M); and between applications in the cloud.

- Cloud computing is the on-demand availability of computer system resources, especially data storage (cloud storage) and computing power, without direct active management by the user. The term is generally used to describe data centers available to many users over the Internet.Edge computing is used in IIOT.


# sensors
- Sensors have been used in different industries for varied purposes for a long time now. 
- The IIoT allows production line managers to monitor their production lines and understand when equipment may require repairs, replacement, or maintenance.So, Vibration – Vibration sensors are exceedingly valuable to an IIoT device.


## Benefits of IIoT in manufacturing and beyond

- One of the greatest benefits of Industrial Internet of Things has to be seen in the reduction of human errors and manual labor, the increase in overall efficiency and the reduction of costs, both in terms of time and money. We also cannot forget the possible underpinnings of IIoT in quality control and maintenance.

1. Improved and intelligent connectivity between devices or machines
2. Increased efficiency
3. Cost savings and
4. Time savings
5. Enhanced industrial safety

![](https://www.i-scoop.eu/wp-content/uploads/2017/01/Most-important-drivers-of-IIoT-adoption-according-to-Morgan-Stanley-source-and-more-information-on-the-survey-here.gif.webp)

## Advantech UNO-420 IIOT Gateway
- It is a (PoE) Powered Device Sensing Gateway with 3 x COM, 2 x LAN (1 x PoE), 8 x GPIO, HDMI, USB3.0,TPM2.0

- The new UNO-420 is ruggedized fanless design with Intel ® AtomTM E3815 Processor, 3x COM port, 2x LAN port (one with Power Over Ethernet (PoE) technology), and 8 x Programmable GPIO. PoE allows you to add a device in awkward or remote locations where power cable may not equipment with. It supports one Ethernet cable for both data and power delivery.Ethernet cable, which appears in many existing infrastructures is considerably less expensive than fiber over the scope of an installation. UNO-420 features optimized I/O, PoE-IN,and wide operation temperature, which is suitable for machine data gateway in many industries.

 [](https://advcloudfiles.advantech.com/cms/3f64f983-36ab-450e-878c-a4195b99bfa2/Resources%20Featured%20Image%20for%20Detail%20Page/UNO-420_test2.jpg)

# Features:

- Intel AtomTM E3815 Processor with 2GB DDR3L onboard memory
- 2 x GbE (1 x PoE-IN), 1 x USB 3.0, 1 x HDMI, 1 x RS-485,
2 x RS-232/422/485, 8 x Programmable GPIO, TPM 2.0
- 32GB eMMC storage onboard
- Support dual wireless communication for WiFi/3G/4G/GPS
- Support one Ethernet cable for both data & power delivery
- Support Trusted Platform Module (TPM) 2.0 for cyber security
- Advantech WISE-PaaS/EdgeLink support
- Edgelink support MQTT, LWM2M client for cloud communication• EdgeLink supports more than 200 I/O Drivers
- EdgeLink supports user-defined acquisition period.
